package com.bekwam.fxc.exceptions;

/**
 * An exception thrown if there is a problem creating the client or service
 *
 * Intended to be fatal
 *
 * @author carl
 */
public class InitializationException extends Exception {

    private static final long serialVersionUID = -7374269941777505689L;

    public InitializationException() {}

    public InitializationException(String message) {
        super(message);
    }
}
