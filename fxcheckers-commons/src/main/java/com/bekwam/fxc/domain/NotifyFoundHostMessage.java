/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.domain;

/**
 * @author carl
 *
 */
public class NotifyFoundHostMessage extends Message {

	private String hostScreenName;
	private String host;
	private int port;
	
	public NotifyFoundHostMessage() {
		super(Message.TYPE_NOTIFY_FOUND_HOST);
	}
	
	public NotifyFoundHostMessage(String hostScreenName, String host, int port) {
		this();
		this.hostScreenName = hostScreenName;
		this.host = host;
		this.port = port;
	}
	
	public String getHostScreenName() {
		return hostScreenName;
	}
	public void setHostScreenName(String hostScreenName) {
		this.hostScreenName = hostScreenName;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
