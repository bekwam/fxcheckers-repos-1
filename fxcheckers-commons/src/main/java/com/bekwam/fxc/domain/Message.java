/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author carl
 *
 */
@XmlRootElement(name="message")
@XmlAccessorType(XmlAccessType.FIELD)
public class Message {

	public static String TYPE_REQUEST_GAME              = "requestGame";
	public static String TYPE_NOTIFY_FOUND_HOST         = "notifyFoundHost";
	public static String TYPE_NOTIFY_FOUND_OTHER_PLAYER = "notifyFoundOtherPlayer";
	public static String TYPE_NOTIFY_HOSTING            = "notifyHosting";
	public static String TYPE_REQUEST_GAME_END          = "requestGameEnd";
	public static String TYPE_NOTIFY_GAME_END           = "notifyGameEnd";
	
	private final String type;

	public Message(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	@Override
	public String toString() {
		return "Message [type=" + type + "]";
	}
}

