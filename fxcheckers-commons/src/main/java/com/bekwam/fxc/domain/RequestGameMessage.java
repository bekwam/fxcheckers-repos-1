/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.domain;

/**
 * @author carl
 *
 */
public class RequestGameMessage extends Message {

	private String screenName;
	
	public RequestGameMessage() {
		super( Message.TYPE_REQUEST_GAME );
	}
	
	public RequestGameMessage(String screenName) {
		this();
		this.screenName = screenName;
	}

	public String getScreenName() {
		return screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	@Override
	public String toString() {
		return "RequestGameMessage [screenName=" + screenName + ", getType()=" + getType() + "]";
	}
}
