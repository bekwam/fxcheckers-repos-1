package com.bekwam.fxc.client;

import javafx.scene.Group;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Rectangle;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for FXCheckersController
 *
 * @author carl
 */
public class FXCheckersControllerTest {

    private FXCheckersController controller;

    @Before
    public void init() {
        controller = new FXCheckersController();
    }

    @Test
    public void needsKingRedOnTop() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.RED);
        OrientationType boardOrientation = OrientationType.RED_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertTrue( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noKingRedOnTop() {

        Pane row = new Pane();
        row.setId("row1");  // not bottom row

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.RED);
        OrientationType boardOrientation = OrientationType.RED_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noKingRedOnTop2() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.RED);
        OrientationType boardOrientation = OrientationType.BLACK_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noRedPieces() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.RED);
        OrientationType boardOrientation = OrientationType.RED_ON_TOP;

        VBox topJumpTray = new VBox();
        VBox bottomJumpTray = new VBox();

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void needsKingBlackOnTop() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.BLACK);
        OrientationType boardOrientation = OrientationType.BLACK_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertTrue( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noKingBlackOnTop() {

        Pane row = new Pane();
        row.setId("row1");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.BLACK);
        OrientationType boardOrientation = OrientationType.BLACK_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noKingBlackOnTop2() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.BLACK);
        OrientationType boardOrientation = OrientationType.RED_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void noBlackPieces() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.BLACK);
        OrientationType boardOrientation = OrientationType.BLACK_ON_TOP;

        VBox topJumpTray = new VBox();
        VBox bottomJumpTray = new VBox();

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }

    @Test
    public void alreadyKinged() {

        Pane row = new Pane();
        row.setId("row0");

        Rectangle square = new Rectangle();

        Optional<PickResult> pickResult = Optional.of( new PickResult(square, row) );

        Group selectedGroup = new Group();
        selectedGroup.getProperties().put(Constants.PROPERTY_SIDE, SideType.RED);
        selectedGroup.getProperties().put(Constants.PROPERTY_SECONDARY_GAMEPIECE, new Group());  // already kinged

        OrientationType boardOrientation = OrientationType.RED_ON_TOP;

        VBox topJumpTray = new VBox();
        topJumpTray.getChildren().add( new Group() );
        VBox bottomJumpTray = new VBox();
        bottomJumpTray.getChildren().add( new Group() );

        assertFalse( controller.checkForKing(pickResult, selectedGroup, boardOrientation, topJumpTray, bottomJumpTray).isPresent() );
    }
}
