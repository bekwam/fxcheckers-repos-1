package com.bekwam.fxc.client;

/**
 * Enum defining player sides
 *
 * @author carl
 */
public enum SideType {

    RED,

    BLACK;

}
