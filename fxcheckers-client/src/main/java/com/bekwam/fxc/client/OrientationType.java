package com.bekwam.fxc.client;

/**
 * Enum describing arrangement of pieces on board
 *
 * @author carl
 */
public enum OrientationType {

    RED_ON_TOP,

    BLACK_ON_TOP;
}
