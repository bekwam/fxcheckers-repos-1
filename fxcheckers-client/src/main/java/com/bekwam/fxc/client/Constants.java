package com.bekwam.fxc.client;

/**
 * @author carl
 */
public interface Constants {

   String PROPERTY_SIDE = "side";
   String PROPERTY_SECONDARY_GAMEPIECE = "secondaryGamePiece";  // kinged
}
