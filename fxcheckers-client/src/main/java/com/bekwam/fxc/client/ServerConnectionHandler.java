/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.client;

import java.lang.ref.WeakReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.bekwam.fxc.domain.Message;
import com.bekwam.fxc.domain.NotifyFoundHostMessage;
import com.bekwam.fxc.domain.NotifyFoundOtherPlayerMessage;
import com.bekwam.fxc.domain.RequestGameMessage;
import com.google.gson.Gson;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketClientProtocolHandler;
import io.netty.util.CharsetUtil;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * @author carl
 *
 */
@Sharable
public class ServerConnectionHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

	private final Logger log = Logger.getLogger("ServerConnectionHandler");

	private final String screenName;
	private final TextField tfPlayer;
	private final Label lblHosting;
	private final WeakReference<FXCheckersController> controllerRef;
	
	public ServerConnectionHandler(String screenName, TextField tfPlayer, Label lblHosting, FXCheckersController controller) {
		this.screenName = screenName;
		this.tfPlayer = tfPlayer;
		this.lblHosting = lblHosting;
		controllerRef = new WeakReference<>( controller );
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext arg0, TextWebSocketFrame in) throws Exception {
		
		final String message = in.text();
		
		if( log.isLoggable(Level.FINE) ) {
			log.fine("[CHANNEL READ0] input=" + message);
		}

   		Message m = new Gson().fromJson( message, Message.class );
   		
   		if( m.getType().equals( Message.TYPE_NOTIFY_FOUND_HOST ) ) {

   			NotifyFoundHostMessage msg = new Gson().fromJson(message,  NotifyFoundHostMessage.class );
   			Platform.runLater(() -> {
   				if( log.isLoggable(Level.FINE) ) {
   					log.fine("[CHANNEL READ0] setting tfPlayer to " + msg.getHostScreenName());
   				}
   				tfPlayer.setText( msg.getHostScreenName() );	
   			});
   			
   		} else if( m.getType().equals( Message.TYPE_NOTIFY_FOUND_OTHER_PLAYER ) ) {
   		
   			NotifyFoundOtherPlayerMessage msg = new Gson().fromJson(message, NotifyFoundOtherPlayerMessage.class );
   			Platform.runLater( () -> {
   				
   				if( log.isLoggable(Level.FINE) ) {
   					log.fine("[CHANNEL READ0] setting tfPlayer to " + msg.getOtherPlayerScreenName());
   				}
   				tfPlayer.setText(msg.getOtherPlayerScreenName() );
   			} );
   		
   		} else if( m.getType().equals( Message.TYPE_NOTIFY_HOSTING ) ) {
   			
   			Platform.runLater( () -> lblHosting.setVisible(true) );
   			
   		} else if( m.getType().equals(Message.TYPE_NOTIFY_GAME_END ) ) {
   			
   			if( controllerRef != null ) {
   				FXCheckersController controller = controllerRef.get();
   				controller.connect();
   			}
   		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		log.log(Level.SEVERE, "error in echo client", cause );
		ctx.close();
	}

	@Override
	public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
	    super.userEventTriggered(ctx, evt);
	    
		if( log.isLoggable(Level.FINE) ) {
			log.fine("[USER EVENT] evt=" + evt);
		}

		if( evt == WebSocketClientProtocolHandler.ClientHandshakeStateEvent.HANDSHAKE_COMPLETE ) {			
			RequestGameMessage msg = new RequestGameMessage(screenName);
			String json = new Gson().toJson( msg );			
	        ChannelFuture f = ctx.channel().writeAndFlush(new TextWebSocketFrame(true, 0, Unpooled.copiedBuffer(json, CharsetUtil.UTF_8)));
	        f.sync();
	    }
	}	
	
}