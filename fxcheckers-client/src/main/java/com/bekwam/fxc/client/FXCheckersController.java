/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.client;

import com.bekwam.fxc.exceptions.InitializationException;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.DefaultHttpHeaders;
import io.netty.handler.codec.http.HttpClientCodec;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.websocketx.WebSocketClientHandshakerFactory;
import io.netty.handler.codec.http.websocketx.WebSocketClientProtocolHandler;
import io.netty.handler.codec.http.websocketx.WebSocketVersion;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.When;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.util.Duration;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.bekwam.fxc.client.Constants.PROPERTY_SIDE;

/**
 * @author carl
 *
 */
public class FXCheckersController {

	private final Logger log = Logger.getLogger("FXCheckersController");

	private final static String PROPERTY_GAMEPIECE =      "gamePiece";
	private final static String PROPERTY_CIRCLE =         "circle";
	private final static String PROPERTY_LEAVING_CIRCLE = "leavingCircle";
	private final static String PROPERTY_TRAY_CIRCLE =    "trayCircle";
	private final static String PROPERTY_KING_IMAGE =     "kingImage";

    private final static int INDEX_TRAY_CIRCLE = 0;
    private final static int INDEX_LEAVING_CIRCLE = 1;
    private final static int INDEX_CIRCLE = 2;

	private final static int SQUARE_SIDE_LEN = 100;

	/*
	 * private final int NUM_ROWS = 8; private final int NUM_COLS = 8; private
	 * final double PADDING = 40;
	 */
	@FXML
	VBox vbox;

	@FXML
	Pane boardPane; // contains circle and rectangleGroup

	@FXML
	Circle circle, kingedCircleBlack;  // template objects
	
	@FXML
	Circle leavingCircle, kingedLeavingCircleBlack; // template object: visual cue on previous space

	@FXML
	Group gamePiece, kingedGamePieceBlack;  // template object: combines circle and leavingCircle

	@FXML
	ImageView kingedImage;

	@FXML
	Group rectangleGroup;

	//
	// bottom row
	//
	@FXML
	Pane row0;
	@FXML
	Rectangle sq0_0;
	@FXML
	Rectangle sq1_0;
	@FXML
	Rectangle sq2_0;
	@FXML
	Rectangle sq3_0;
	@FXML
	Rectangle sq4_0;
	@FXML
	Rectangle sq5_0;
	@FXML
	Rectangle sq6_0;
	@FXML
	Rectangle sq7_0;

	//
	// 2nd from bottom
	//
	@FXML
	Pane row1;
	@FXML
	Rectangle sq0_1;
	@FXML
	Rectangle sq1_1;
	@FXML
	Rectangle sq2_1;
	@FXML
	Rectangle sq3_1;
	@FXML
	Rectangle sq4_1;
	@FXML
	Rectangle sq5_1;
	@FXML
	Rectangle sq6_1;
	@FXML
	Rectangle sq7_1;

	//
	// 3rd from bottom
	//
	@FXML
	Pane row2;
	@FXML
	Rectangle sq0_2;
	@FXML
	Rectangle sq1_2;
	@FXML
	Rectangle sq2_2;
	@FXML
	Rectangle sq3_2;
	@FXML
	Rectangle sq4_2;
	@FXML
	Rectangle sq5_2;
	@FXML
	Rectangle sq6_2;
	@FXML
	Rectangle sq7_2;

	//
	// 4th from bottom
	//
	@FXML
	Pane row3;
	@FXML
	Rectangle sq0_3;
	@FXML
	Rectangle sq1_3;
	@FXML
	Rectangle sq2_3;
	@FXML
	Rectangle sq3_3;
	@FXML
	Rectangle sq4_3;
	@FXML
	Rectangle sq5_3;
	@FXML
	Rectangle sq6_3;
	@FXML
	Rectangle sq7_3;

	//
	// 4th from top
	//
	@FXML
	Pane row4;
	@FXML
	Rectangle sq0_4;
	@FXML
	Rectangle sq1_4;
	@FXML
	Rectangle sq2_4;
	@FXML
	Rectangle sq3_4;
	@FXML
	Rectangle sq4_4;
	@FXML
	Rectangle sq5_4;
	@FXML
	Rectangle sq6_4;
	@FXML
	Rectangle sq7_4;

	//
	// 3rd from top
	//
	@FXML
	Pane row5;
	@FXML
	Rectangle sq0_5;
	@FXML
	Rectangle sq1_5;
	@FXML
	Rectangle sq2_5;
	@FXML
	Rectangle sq3_5;
	@FXML
	Rectangle sq4_5;
	@FXML
	Rectangle sq5_5;
	@FXML
	Rectangle sq6_5;
	@FXML
	Rectangle sq7_5;

	//
	// 2nd from top
	//
	@FXML
	Pane row6;
	@FXML
	Rectangle sq0_6;
	@FXML
	Rectangle sq1_6;
	@FXML
	Rectangle sq2_6;
	@FXML
	Rectangle sq3_6;
	@FXML
	Rectangle sq4_6;
	@FXML
	Rectangle sq5_6;
	@FXML
	Rectangle sq6_6;
	@FXML
	Rectangle sq7_6;

	//
	// top row
	//
	@FXML
	Pane row7;
	@FXML
	Rectangle sq0_7;
	@FXML
	Rectangle sq1_7;
	@FXML
	Rectangle sq2_7;
	@FXML
	Rectangle sq3_7;
	@FXML
	Rectangle sq4_7;
	@FXML
	Rectangle sq5_7;
	@FXML
	Rectangle sq6_7;
	@FXML
	Rectangle sq7_7;

	@FXML
	TextField tfPlayer;

	@FXML
	TextField tfScreenName;
	
	@FXML
	Button btnConnect;
	
	@FXML
	Label lblHosting;

	@FXML
	VBox jumpTrayContainer;

    @FXML
    VBox topJumpTray;  // RED pieces if RED_ON_TOP, BLACK pieces if BLACK_ON_TOP

    @FXML
    VBox bottomJumpTray;  // BLACK pieces if RED_ON_TOP; RED pieces of BLACK_ON_TOP

    @FXML
    Circle redTrayPieceTemplate;

    @FXML
    Circle blackTrayPieceTemplate;

	@FXML
	Circle turnIndicator;

	private final List<Pane> panes = new ArrayList<>();
	private final InnerShadow shadow = new InnerShadow();
	private Rectangle currRect;
	private Point2D offset = new Point2D(0.0d, 0.0d);
	private boolean movingPiece = false;
	private Channel serverChannel;
	private BooleanProperty connectedToServer = new SimpleBooleanProperty(false);
	private EventLoopGroup group;	
	private Set<String> blackSquareIds = new LinkedHashSet<>();  // can't put a piece on these
	
	private Circle selectedCircle;
	private Circle selectedLeavingCircle;
	private Group selectedGamePiece;
	private ImageView selectedKingImage ;

	private OrientationType boardOrientation = OrientationType.RED_ON_TOP;

	private double circleCenteringOffsetX = 0.0d;
	private double circleCenteringOffsetY = 0.0d;
	private double leavingCircleCenteringOffsetX = 0.0d;
	private double leavingCircleCenteringOffsetY = 0.0d;

	private ObjectProperty<SideType> turn = new SimpleObjectProperty<>();

	private boolean aMoveWasMade = false;
	private Optional<Group> lastJumperGamePiece = Optional.empty();

	@FXML
	public void initialize() throws Exception {

		initTurn();
		initConstants();

		initPanes();
		initBoardPane();

	//	vbox.setMaxWidth(800.0d);

		initConnectControls();

		initJumpTray();

		createBlackSquareIds();
		createGamePieces();

		initBoard();
	}

	private void initBoard() {
		boardPane.setOnMouseEntered((evt) -> boardPane.requestFocus());  // don't put key commands in the TextFields
	}

	private void initConnectControls() {
		btnConnect.textProperty().bind(
					new When(connectedToServer)
						.then("Disconnect")
						.otherwise("Connect")
						);

		tfScreenName.editableProperty().bind(connectedToServer.not());

		lblHosting.setVisible( false );
	}

	private void initJumpTray() {
		topJumpTray.getChildren().remove( redTrayPieceTemplate );
		bottomJumpTray.getChildren().remove( blackTrayPieceTemplate );
	}

	private void initConstants() {
		//
		// some computations saved for later usage
		//
		circleCenteringOffsetX = sq0_0.getWidth()/2 - circle.getRadius();
		circleCenteringOffsetY = sq0_0.getHeight()/2 - circle.getRadius();
		leavingCircleCenteringOffsetX = sq0_0.getWidth()/2 - leavingCircle.getRadius();
		leavingCircleCenteringOffsetY = sq0_0.getHeight()/2 - leavingCircle.getRadius();
	}

	private void initTurn() {
		turnIndicator.fillProperty().bind( new When(turn.isEqualTo(SideType.RED)).then(Color.RED).otherwise(Color.BLACK));
		turn.setValue( SideType.RED );
	}

	private void initPanes() {
		panes.add(row0);
		panes.add(row1);
		panes.add(row2);
		panes.add(row3);
		panes.add(row4);
		panes.add(row5);
		panes.add(row6);
		panes.add(row7);
	}

	private void initBoardPane() {
		boardPane.addEventFilter(MouseEvent.MOUSE_EXITED, this::leaveBoard);
		boardPane.addEventFilter(MouseEvent.MOUSE_RELEASED, this::checkReleaseOutOfBoard);
		boardPane.setOnMouseMoved(this::mouseMoved);
		boardPane.setOnMouseDragged(this::mouseMoved);
		boardPane.setOnMousePressed(this::startMovingPiece);
		boardPane.setOnMouseReleased(this::finishMovingPiece);
	}

	private void createGamePieces() throws InitializationException {

		//
		// Using gamePiece, circle and leavingCircle as templates, create
		// other gamePieces
		//
		
		// remove template pieces
		boardPane.getChildren().remove( gamePiece );
		boardPane.getChildren().remove( kingedGamePieceBlack );

		// add red pieces
		for( int i=0; i<=700; i=i+100 ) {

			if( i == 300 || i == 400 ) {
				continue;  // no pieces on this row
			}

			for( int j=0; j<800; j=j+200 ) {

				//
				// red offsets : row1 = 0, row2 = 100; row3 = 0
				//
				// black offsets: row5 = 100, row6 = 0, row7 = 100
				//
				int boardOffset = (i==100||i==500||i==700)?100:0;

				//
				// Identifiers are based on original position
				//
				// relocate() calls include offset and centering computation
				//
				Group newGamePiece = new Group();
				newGamePiece.setId("gr" + (i/100) + "_" + (j/100));

				Circle templ = null;
				if( i == 0 || i == 100 || i == 200 ) {  // top three rows are red
					newGamePiece.getProperties().put(PROPERTY_SIDE, SideType.RED);
					templ = redTrayPieceTemplate;
				} else {  // middle rows were skipped with above continue statement
					newGamePiece.getProperties().put(PROPERTY_SIDE, SideType.BLACK);
					templ = blackTrayPieceTemplate;
				}

				Circle newCircle=createCircleFromTemplate( circle );
				newCircle.setId( "ci" + (i/100) + "_" + (j/100) );

				if( i == 500 || i == 600 || i == 700 ) {
					newCircle.setFill(Color.BLACK);
					newCircle.setStroke(Color.BLACK);
				}

				Circle newTrayCircle = createCircleFromTemplate( templ );
                newTrayCircle.setId( "tc" + (i/100) + "_" + (j/100) );
                newTrayCircle.setVisible(false);  // only displays in the tray

				Circle newLeavingCircle = createCircleFromTemplate( leavingCircle );
				newLeavingCircle.setOpacity( 0 );
				newLeavingCircle.setId( "lc" + (i/100) + "_" + (j/100) );
				newLeavingCircle.setTranslateX( circle.getRadius()-newLeavingCircle.getRadius() );
				newLeavingCircle.setTranslateY( circle.getRadius()-newLeavingCircle.getRadius() );

				ImageView newKingImage = createKingImage();
				newKingImage.setId( "ki" + (i/100) + "_" + (j/100));

//				newKingImage.setOpacity(0.0d);

				newGamePiece.getProperties().put( PROPERTY_CIRCLE, newCircle );
				newGamePiece.getProperties().put( PROPERTY_LEAVING_CIRCLE, newLeavingCircle );
				newGamePiece.getProperties().put( PROPERTY_TRAY_CIRCLE, newTrayCircle );
				newGamePiece.getProperties().put( PROPERTY_KING_IMAGE, newKingImage );

				newGamePiece.getChildren().addAll( newTrayCircle, newLeavingCircle, newCircle );

				boardPane.getChildren().add( newGamePiece );

				//
				// Using newCircle's radius is ok b/c newTrayCircle, newLeavingCircle hidden behind
				//
				newGamePiece.relocate( j+boardOffset+circleCenteringOffsetX, i+circleCenteringOffsetY );

				//
				// Associate piece with square for later logic
				//
				Point2D layoutScene = newGamePiece.localToScene(
						new Point2D(newCircle.getCenterX(), newCircle.getCenterY())
						);

				Optional<PickResult> pickResultOpt = pickRectangle(layoutScene.getX(), layoutScene.getY());

				PickResult pickResult = pickResultOpt.orElseThrow( () -> {
					String msg = "unable to match game piece " + newGamePiece.getId() + " with an underlying square";
					if (log.isLoggable(Level.SEVERE)) {
						log.severe(msg);
					}
					return new InitializationException(msg);
				});

				Rectangle overSquare = pickResult.getSquare();
				overSquare.getProperties().put(PROPERTY_GAMEPIECE, newGamePiece);
			}
		}
	}

	private Circle createCircleFromTemplate(Circle templateCircle) {
		
		Circle newCircle = new Circle();
		
		newCircle.setRadius( templateCircle.getRadius() );
		newCircle.setLayoutX( templateCircle.getLayoutX() );
		newCircle.setLayoutY( templateCircle.getLayoutY() );
		newCircle.setCenterX( templateCircle.getCenterX() );
		newCircle.setCenterY( templateCircle.getCenterY() );
		newCircle.setFill( templateCircle.getFill() );
		newCircle.setStroke( templateCircle.getStroke() );
		
		return newCircle;
	}
	
	private void createBlackSquareIds() {
		
		blackSquareIds.add( "sq1_7" );
		blackSquareIds.add( "sq3_7" );
		blackSquareIds.add( "sq5_7" );
		blackSquareIds.add( "sq7_7" );
		
		blackSquareIds.add( "sq0_6" );
		blackSquareIds.add( "sq2_6" );
		blackSquareIds.add( "sq4_6" );
		blackSquareIds.add( "sq6_6" );

		blackSquareIds.add( "sq1_5" );
		blackSquareIds.add( "sq3_5" );
		blackSquareIds.add( "sq5_5" );
		blackSquareIds.add( "sq7_5" );

		blackSquareIds.add( "sq0_4" );
		blackSquareIds.add( "sq2_4" );
		blackSquareIds.add( "sq4_4" );
		blackSquareIds.add( "sq6_4" );

		blackSquareIds.add( "sq1_3" );
		blackSquareIds.add( "sq3_3" );
		blackSquareIds.add( "sq5_3" );
		blackSquareIds.add( "sq7_3" );

		blackSquareIds.add( "sq0_2" );
		blackSquareIds.add( "sq2_2" );
		blackSquareIds.add( "sq4_2" );
		blackSquareIds.add( "sq6_2" );

		blackSquareIds.add( "sq1_1" );
		blackSquareIds.add( "sq1_3" );
		blackSquareIds.add( "sq1_5" );
		blackSquareIds.add( "sq1_7" );

		blackSquareIds.add( "sq0_0" );
		blackSquareIds.add( "sq2_0" );
		blackSquareIds.add( "sq4_0" );
		blackSquareIds.add( "sq6_0" );

	}
	
	public void checkReleaseOutOfBoard(MouseEvent evt) {
		Point2D mousePoint_s = new Point2D(evt.getSceneX(), evt.getSceneY());
		if (!inBoard(mousePoint_s)) {
			leaveBoard(evt);
			evt.consume();
		}
	}

	public void leaveBoard(MouseEvent evt) {
		if (movingPiece) {

			final Timeline timeline = new Timeline();

			offset = new Point2D(0.0d, 0.0d);
			movingPiece = false;

			timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
					new KeyValue(selectedCircle.layoutXProperty(), selectedLeavingCircle.getLayoutX()),
					new KeyValue(selectedCircle.layoutYProperty(), selectedLeavingCircle.getLayoutY()),
					new KeyValue(selectedCircle.opacityProperty(), 1.0d), new KeyValue(selectedLeavingCircle.opacityProperty(), 0.0d)));
			timeline.play();
		}
	}

	@FXML
	public void startMovingPiece(MouseEvent evt) {	

		Optional<Group> selg = pickGamePiece(evt);

		selg.ifPresent( (sg) -> {

			SideType selectedGPSide = (SideType)sg.getProperties().get( PROPERTY_SIDE );

			if( selectedGPSide.equals(turn.get()) ) {

				if( !aMoveWasMade || (aMoveWasMade && lastJumperGamePiece.isPresent() && lastJumperGamePiece.get().equals(sg))) {

					selectedGamePiece = sg;
					selectedCircle = (Circle) selectedGamePiece.getProperties().get(PROPERTY_CIRCLE);
					selectedLeavingCircle = (Circle) selectedGamePiece.getProperties().get(PROPERTY_LEAVING_CIRCLE);
					selectedKingImage = (ImageView) selectedGamePiece.getProperties().get(PROPERTY_KING_IMAGE);

					selectedCircle.setOpacity(0.4d);

					Point2D scenePt = new Point2D(evt.getSceneX(), evt.getSceneY());
					Point2D localPt = selectedCircle.sceneToLocal(scenePt);

					offset = new Point2D(localPt.getX() - 100, localPt.getY());  // TODO: check where the 100 came from

					selectedLeavingCircle.setOpacity(1.0d);
					selectedLeavingCircle.setLayoutX(selectedCircle.getLayoutX());
					selectedLeavingCircle.setLayoutY(selectedCircle.getLayoutY());

					if (log.isLoggable(Level.FINE)) {
						log.fine("[START] selected gp=" + selectedGamePiece.getId());
					}

					movingPiece = true;
				} else {
					if( log.isLoggable(Level.FINE) ) {
						log.fine("[START] in the middle of a turn, but didn't select the same piece to try for add'l jumps");
					}
				}
			} else {

				if( log.isLoggable( Level.FINE) ) {
					log.fine("[START] attempted to move a " + selectedGPSide + " piece when turn is " + turn.get() );
				}
			}
		});
	}

	public void mouseMoved(MouseEvent evt) {

		Optional<PickResult> pickResult = pickRectangle(evt.getSceneX(), evt.getSceneY());
		Rectangle r = null;

		if (!pickResult.isPresent()) {

			if (currRect != null) {
				// deselect previous
				currRect.setEffect(null);
			}

			currRect = null;

			// might be out of area but w/i scene

		} else {

			r = pickResult.get().getSquare();

			if (r != currRect) {

				if (currRect != null) {
					// deselect previous
					currRect.setEffect(null);
				}

				currRect = r;

				if (log.isLoggable(Level.FINEST)) {
					log.finest("[MOVED] in " + currRect.getId());
				}

				if (currRect != null) { // new selection
					if (!blackSquareIds.contains(currRect.getId())) { // can't put piece on black square
						currRect.setEffect(shadow);
					}
				}
			}

			if (movingPiece) {
				Point2D parentMP = selectedGamePiece.parentToLocal( evt.getX(), evt.getY() );
				if( log.isLoggable(Level.FINEST) ) {
					log.finest("[MOVED] moving to " + (parentMP.getX()-offset.getX()) + ", " + (parentMP.getY()-offset.getY()));
				}
				selectedCircle.relocate( parentMP.getX()-offset.getX(), parentMP.getY()-offset.getY());
			}
		}
	}

	@FXML
	public void finishMovingPiece(MouseEvent evt) {

		if( !movingPiece ) {
			return;  // the user attempted to select a piece out of turn
		}

		Group jumpedPiece = null;
		boolean needsMoveToTray = false;
		Point2D mousePointScene = new Point2D( evt.getSceneX(), evt.getSceneY() );

		Optional<PickResult> pickResult = pickRectangle(mousePointScene.getX(), mousePointScene.getY());

		Rectangle r = null;   // also a flag indicating that a move took place
		if( pickResult.isPresent() ) {
			r = pickResult.get().getSquare();
		}

		if( r != null ) {
            boolean legalMove = isLegalMove( mousePointScene, r );
            if( !legalMove ) {

                jumpedPiece = isJump( r );

                if( jumpedPiece == null ) {

                    if (log.isLoggable(Level.FINE)) {
                        log.fine("[FINISH] move not legal and not jump");
                    }
                    r = null;  // no valid Rectangle to land on

				} else {

                    if (isLegalMoveDirection(mousePointScene)) {
                        needsMoveToTray = true;
                    } else {
						if( log.isLoggable(Level.FINE) ) {
							log.fine("[FINISH] not a legal move direction");
						}
						r = null;
					}
                }

            } else {

				// move can go on unless only a double (or triple) jump is allowed at this point
				if( aMoveWasMade ) {
					if( log.isLoggable(Level.FINE) ) {
						log.fine("[FINISH] not a legal move b/c only a jump continuation is permitted here");
					}
					r = null;  // not a valid move for a continuation
				}
			}
		}
		
		final Timeline timeline = new Timeline();
		timeline.setCycleCount(1);
		timeline.setAutoReverse(false);

		if (r != null) {

			Point2D rectScene = r.localToScene(r.getX(), r.getY());

			if (log.isLoggable(Level.FINE)) {
				log.fine("[FINISH] moving on to r=" + r.getId());
			}

			Point2D bp = boardPane.sceneToLocal( rectScene );

			Point2D parent = selectedGamePiece.parentToLocal(bp);
			Bounds layoutBoundsC = selectedCircle.getLayoutBounds();
			Bounds layoutBoundsLC = selectedLeavingCircle.getLayoutBounds();

			if (log.isLoggable(Level.FINE)) {
				log.fine("[FINISH] moving selected circle to (" +
						(parent.getX()-layoutBoundsC.getMinX()+circleCenteringOffsetX) + ", " +
						(parent.getY()-layoutBoundsC.getMinY()+circleCenteringOffsetY) + ")");
			}

			timeline.getKeyFrames()
					.add(new KeyFrame(Duration.millis(100),
							new KeyValue(selectedCircle.layoutXProperty(), parent.getX()-layoutBoundsC.getMinX()+circleCenteringOffsetX),
							new KeyValue(selectedCircle.layoutYProperty(), parent.getY()-layoutBoundsC.getMinY()+circleCenteringOffsetY),
							new KeyValue(selectedCircle.opacityProperty(), 1.0d),
							new KeyValue(selectedLeavingCircle.layoutXProperty(), parent.getX()-layoutBoundsLC.getMinX()+leavingCircleCenteringOffsetX),
							new KeyValue(selectedLeavingCircle.layoutYProperty(), parent.getY()-layoutBoundsLC.getMinY()+ leavingCircleCenteringOffsetY),
							new KeyValue(selectedLeavingCircle.opacityProperty(), 0.0d)));

		} else {

			if( log.isLoggable( Level.FINE ) ) {
				log.fine("[FINISH] returning selected circle to original spot");
			}
			timeline.getKeyFrames().add(new KeyFrame(Duration.millis(200),
					new KeyValue(selectedCircle.layoutXProperty(), selectedLeavingCircle.getLayoutX()),
					new KeyValue(selectedCircle.layoutYProperty(), selectedLeavingCircle.getLayoutY()),
					new KeyValue(selectedCircle.opacityProperty(), 1.0d),
					new KeyValue(selectedLeavingCircle.opacityProperty(), 0.0d)));
		}

		Point2D leavingCircleCenter = new Point2D( selectedLeavingCircle.getCenterX(), selectedLeavingCircle.getCenterY() );
		Point2D leavingCircleCenterScene = selectedLeavingCircle.localToScene(leavingCircleCenter);

		Optional<PickResult> srcSquarePickResult = pickRectangle( leavingCircleCenterScene.getX(), leavingCircleCenterScene.getY() );

		final Rectangle targetRectangle = r;  // might be null
		final boolean needsMoveToTray_f = needsMoveToTray;
		final Group jumpedPiece_f = jumpedPiece;

		timeline.play();
		timeline.setOnFinished((finishedEvt) -> {

			if( needsMoveToTray_f ) {

				moveToTray(jumpedPiece_f);

				lastJumperGamePiece = Optional.of(selectedGamePiece);

			}

			if( targetRectangle != null ) {  // there was a move

				Rectangle sourceRectangle = null;
				if( srcSquarePickResult.isPresent() ) {
					sourceRectangle = srcSquarePickResult.get().getSquare();
				}

				if( log.isLoggable(Level.FINE) ) {
                    log.fine("[FINISH] clearing rect=" + sourceRectangle.getId() + " and setting target rect=" + targetRectangle.getId());
					log.fine("[FINISH] there was a move");
                }

				aMoveWasMade = true;  // turn continues b/c possibly additional jumps

                sourceRectangle.getProperties().remove( PROPERTY_GAMEPIECE );
                targetRectangle.getProperties().put( PROPERTY_GAMEPIECE, selectedGamePiece );

				//
				// Check for king
				//

				Optional<Group> kingPiece = checkForKing(pickResult, selectedGamePiece, boardOrientation, topJumpTray, bottomJumpTray);
				if( log.isLoggable( Level.FINE ) ) {
					log.fine("[FINISH] needsKing?=" + kingPiece.isPresent());
				}

				kingPiece.ifPresent((kp) -> {

					selectedGamePiece.getProperties().put( Constants.PROPERTY_SECONDARY_GAMEPIECE, kp );

//					selectedGamePiece.getChildren().add( createKingImage() );
					// TODO: set opacity
				});
			}

			if( log.isLoggable(Level.FINE) ) {
				log.fine("[FINISH] ending dimensions of gp=" + selectedGamePiece.getLayoutBounds());
			}

			offset = new Point2D(0.0d, 0.0d);

			movingPiece = false;

			selectedCircle = null;
			selectedLeavingCircle = null;
			selectedGamePiece = null;
			selectedKingImage = null;

		});
	}

	private ImageView createKingImage() {

		ImageView iv = new ImageView();
		iv.setImage( kingedImage.getImage() );
		iv.setFitHeight( kingedImage.getFitHeight() );
		iv.setFitWidth( kingedImage.getFitWidth() );
		iv.setLayoutX( kingedImage.getLayoutX() );
		iv.setLayoutY( kingedImage.getLayoutY() );

		return iv;
	}

	Optional<Group> checkForKing(Optional<PickResult> pickResult, Group sgp, OrientationType bo, VBox topJumpTray, VBox bottomJumpTray) {

		SideType typeOfPiece = (SideType)sgp.getProperties().get(PROPERTY_SIDE);
		Optional<Group> kingPiece = Optional.empty();

		//
		// If already kinged, skip the rest
		//
		if( !sgp.getProperties().containsKey(Constants.PROPERTY_SECONDARY_GAMEPIECE) ) {

			//
			// Make sure that the piece has entered the top or bottom row as specified by the board orientation and that
			// there is a piece in the tray
			//
			if (typeOfPiece.equals(SideType.RED) && bo.equals(OrientationType.RED_ON_TOP) ||
					typeOfPiece.equals(SideType.BLACK) && bo.equals(OrientationType.BLACK_ON_TOP)) {

				if (pickResult.get().getRow().getId().equalsIgnoreCase("row0")) {
					if (!topJumpTray.getChildren().isEmpty()) {
						Group gp = (Group) topJumpTray.getChildren().remove(0);
						kingPiece = Optional.of(gp);
					}
				}

			} else {
				if (pickResult.get().getRow().getId().equalsIgnoreCase("row7")) {
					if (!bottomJumpTray.getChildren().isEmpty()) {
						Group gp = (Group) bottomJumpTray.getChildren().remove(0);
						kingPiece = Optional.of(gp);
					}
				}
			}
		}

		if( log.isLoggable( Level.FINE ) ) {
			log.fine("[FINISH] needsKing?=" + kingPiece.isPresent());
		}

		return kingPiece;
	}

	private boolean inBoard(Point2D pt) {
		Point2D panePt = boardPane.sceneToLocal(pt);
		return panePt.getX()  >= 0.0d && panePt.getY() >= 0.0d
				&& panePt.getX() <= boardPane.getWidth() && panePt.getY() <= boardPane.getHeight();
	}

	private Optional<Group> pickGamePiece(MouseEvent evt) { return pickGamePiece(evt.getX(), evt.getY()); }

	private Optional<Group> pickGamePiece(double x, double y ) {
		Point2D mp = new Point2D(x, y);
		for( Node n : boardPane.getChildren() ) {
			Point2D mpLocal = n.parentToLocal( mp );
			if( StringUtils.startsWith( n.getId(), "gr" ) ) {
				if( n.contains(mpLocal) ) {
					return Optional.of((Group)n);
				}
			}
		}
		return Optional.empty();
	}

	private  Optional<PickResult> pickRectangle(double sceneX, double sceneY) {

		Optional<PickResult> pickResult = Optional.empty();
		Pane pickedRow = null;
		Rectangle pickedRectangle = null;

		for (Pane row : panes) {

			//
			// getX/Y == getSceneX/Y because handler registered on Scene and
			// not node
			//

			Point2D mousePoint = new Point2D(sceneX, sceneY);
			Point2D mpLocal = row.sceneToLocal(mousePoint);

			if (row.contains(mpLocal)) {

				if (log.isLoggable(Level.FINEST)) {
					log.finest("[PICK] selected row=" + row.getId());
				}

				for (Node cell : row.getChildrenUnmodifiable()) {

					Point2D mpLocalCell = cell.sceneToLocal(mousePoint);

					if (cell.contains(mpLocalCell)) {
						if( !blackSquareIds.contains(cell.getId()) ) {
							if (log.isLoggable(Level.FINEST)) {
								log.finest("[PICK] selected cell=" + cell.getId());
							}
							pickedRectangle = (Rectangle) cell;
							pickedRow = row;
							pickResult = Optional.of( new PickResult(pickedRectangle, pickedRow) );
						}
						break;
					}
				}
				break;
			}
		}
		return pickResult;
	}

	@FXML
	public void connect() throws InterruptedException, IOException, URISyntaxException {

		final String host = "localhost";
		final int port = 8080;
		final String screenName = tfScreenName.getText();
		
		if( !connectedToServer.getValue() ) {
			
			group = new NioEventLoopGroup();
					  
			final WebSocketClientProtocolHandler handler =
					  new WebSocketClientProtocolHandler(
							  WebSocketClientHandshakerFactory.newHandshaker(
									  new URI("ws://" + host + "/fxcheckers-server/fxc"), WebSocketVersion.V13, null, false, new DefaultHttpHeaders())
							  );

			Task<Channel> task = new Task<Channel>() {

				@Override
				protected Channel call() throws Exception {
					
					updateMessage("Bootstrapping");
					updateProgress(0.1d, 1.0d);
					
					Bootstrap b = new Bootstrap();
					b
						.group(group)
						.channel(NioSocketChannel.class)
						.remoteAddress( new InetSocketAddress(host, port) )
						.handler( new ChannelInitializer<SocketChannel>() {
							@Override
							protected void initChannel(SocketChannel ch) throws Exception {
								ChannelPipeline p = ch.pipeline();
								p.addLast(new HttpClientCodec());
								p.addLast(new HttpObjectAggregator(8192));
								p.addLast(handler);
								p.addLast(new ServerConnectionHandler(screenName, tfPlayer, lblHosting, FXCheckersController.this));
							}
						});
					
					updateMessage("Connecting");
					updateProgress(0.2d, 1.0d);

					ChannelFuture f = b.connect();				
					f.sync();
					Channel chn = f.channel();
					
					if (log.isLoggable(Level.FINE)) {
						log.fine("[CONNECT] channel active=" + chn.isActive() + ", open=" + chn.isOpen() + ", register=" + chn.isRegistered() + ", writeable=" + chn.isWritable());
					}
					
					return chn;
				}

				@Override
				protected void succeeded() {					
					serverChannel = getValue();					
					connectedToServer.set(true);
				}

				@Override
				protected void failed() {
					
					connectedToServer.set(false);

					Throwable exc = getException();
					exc.printStackTrace();
					log.log(Level.SEVERE, "client connect error", exc );
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Client");
					alert.setHeaderText( exc.getClass().getName() );
					alert.setContentText( exc.getMessage() );
					alert.showAndWait();
					
				}
			};
			
			new Thread(task).start();
			
		} else {
			
			if (log.isLoggable(Level.FINE)) {
				log.fine("[CONNECT] disconnecting");
			}

			Task<Void> task = new Task<Void>() {

				@Override
				protected Void call() throws Exception {
					
					updateMessage("Disconnecting");
					updateProgress(0.1d, 1.0d);
					
					disconnect();

					return null;
				}

				@Override
				protected void succeeded() {
					
					connectedToServer.set(false);
					
					lblHosting.setVisible( false );  // not always needed
					
					tfPlayer.setText("No one");
				}

				@Override
				protected void failed() {
					
					connectedToServer.set(true);

					lblHosting.setVisible( false );  // not always needed

					tfPlayer.setText("No one");

					Throwable t = getException();
					log.log(Level.SEVERE, "client disconnect error", t );
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Client");
					alert.setHeaderText( t.getClass().getName() );
					alert.setContentText( t.getMessage() );
					alert.showAndWait();
				}
				
			};
			
			new Thread(task).start();
		}		
	}
	
	public void disconnect() throws Exception {
		if( serverChannel != null && serverChannel.isOpen()) {	
			serverChannel.close().sync();
		}
		if( group != null ) {
			group.shutdownGracefully().sync();
		}
	}
	
	private boolean isLegalMove(Point2D ptInScene, Rectangle targetSquare) {
		
		//
		// moving in proper direction?
		//
		// Using original position as stored in leavingCircle as reference
		// point
		//
		if( !isLegalMoveDirection(ptInScene) ) {
			if( log.isLoggable(Level.FINE) ) {
				log.fine("[IS LEGAL] can't move backwards");
			}
			return false;
		}
		
		//
		// Check for jump
		//

		//
		// is square occupied?
		//
		if( !isUnoccupied(targetSquare) ) {
			if( log.isLoggable(Level.FINE) ) {
				log.fine("[IS LEGAL] target square is occupied");
			}
			return false;
		}

		//
		// moving to diagonal square? startingSquare -> targetSquare
		//
		// up vs. down handled in previous code blocks
		//		
		if( !isLegalMoveDistance(targetSquare) ) {
			if( log.isLoggable(Level.FINE) ) {
				log.fine("[IS LEGAL] can't move farther than one square (unless jump)");
			}
			return false;
		}

		return true;
	}

	private Group isJump(Rectangle targetSquare) {

		if( targetSquare.getProperties().containsKey(PROPERTY_GAMEPIECE) ) {
			if( log.isLoggable(Level.FINE) ) {
				log.fine("[IS JUMP] targetSquare already contains a piece");
			}
			return null;
		}

        double slcCenterX = selectedLeavingCircle.getCenterX();
        double slcCenterY = selectedLeavingCircle.getCenterY();
        Point2D slcCenterScene = selectedLeavingCircle.localToScene( new Point2D(slcCenterX, slcCenterY) );

		Optional<PickResult> pickResult = pickRectangle(
				slcCenterScene.getX(),
				slcCenterScene.getY()
		);

		Rectangle startingSquare = null;

		if( pickResult.isPresent() ) {
			startingSquare = pickResult.get().getSquare();
		}

        String startingSquareId = startingSquare.getId();
        String startingSquareCoords = StringUtils.substring(startingSquareId, 2, 5);
        String[] startingSquareToks = StringUtils.split(startingSquareCoords, "_");
        Integer startingSqX = NumberUtils.createInteger( startingSquareToks[0] );
        Integer startingSqY = NumberUtils.createInteger( startingSquareToks[1] );

        String targetSquareId = targetSquare.getId();
        String targetSquareCoords = StringUtils.substring(targetSquareId, 2, 5);
        String[] targetSquareToks = StringUtils.split(targetSquareCoords, "_");
        Integer targetSqX = NumberUtils.createInteger( targetSquareToks[0] );
        Integer targetSqY = NumberUtils.createInteger( targetSquareToks[1] );

        int distanceX = Math.abs(targetSqX - startingSqX);
        int distanceY = Math.abs(targetSqY - startingSqY);

        if( log.isLoggable( Level.FINE ) ) {
            log.fine("[IS JUMP] " + startingSquareId + " to " + targetSquareId );
            log.fine("[IS JUMP] distanceX=" + distanceX + ", distanceY=" + distanceY);
        }

        //
        // Must be two squares away and only 2 squares away in both directions; does not handle direction
        //
        if( distanceX == 2 && distanceY == 2 ) {

            Integer middleSqX = (startingSqX + targetSqX)/2;
            Integer middleSqY = (startingSqY + targetSqY)/2;

            String middleSquareId = "sq" + middleSqX + "_"+ middleSqY;

            if( log.isLoggable( Level.FINE ) ) {
                log.fine("[IS JUMP] checking " + middleSquareId + " for opposing piece");
            }

            Rectangle middleSq = findSquareById( middleSquareId );

            //
            // Can't jump own color
            //
            Group gp = (Group)middleSq.getProperties().get(PROPERTY_GAMEPIECE);
            if( gp != null ) {  // there is a piece in between
                SideType typeOfPiece = (SideType)selectedGamePiece.getProperties().get(PROPERTY_SIDE);
                SideType typeOfJumpee = (SideType)gp.getProperties().get(PROPERTY_SIDE);
                if( !typeOfPiece.equals(typeOfJumpee) ) {
					if( log.isLoggable(Level.FINE) ) {
						log.fine("[IS JUMP] color check ok");
					}
                    return gp;  // RED != BLACK or BLACK != RED
                } else {
					if( log.isLoggable( Level.FINE ) ) {
						log.fine("[IS JUMP] trying to jump own color");
					}
				}
            }

        }

        return null;
	}

	private boolean isLegalMoveDirection(Point2D ptInScene) {
		
		double ptY = ptInScene.getY();
		double maxYOfLeavingCircle = selectedLeavingCircle.localToScene(
				selectedLeavingCircle.getLayoutBounds()
				).getMaxY();
		
		if( log.isLoggable(Level.FINE) ) {
			log.fine("[IS LEGAL DIR] ptY=" + ptY + ", maxY=" + maxYOfLeavingCircle);
		}
		
		if( boardOrientation == OrientationType.RED_ON_TOP ) {
			
			if( selectedGamePiece.getProperties().get(PROPERTY_SIDE) == SideType.RED ) {
				
				// red (non-kinged) should only move down
				if( ptY < maxYOfLeavingCircle ) {
					return false;
				} 
				
			} else {  // BLACK
				
				// black (non-kinged) should only move up				
				if( ptY >= maxYOfLeavingCircle ) {
					return false;
				} 

			}
			
		} else {  // BLACK_ON_TOP
			
			if( selectedGamePiece.getProperties().get(PROPERTY_SIDE) == SideType.RED ) {
				// red (non-kinged) should only move up
				if( ptY >= maxYOfLeavingCircle ) {
					return false;
				} 
			} else { // BLACK
				// black (non-kinged) should only move down
				if( ptY < maxYOfLeavingCircle ) {
					return false;
				} 
			}
		}
		
		return true;
	}
	
	private boolean isLegalMoveDistance(Rectangle targetSquare) {
		
		double slcCenterX = selectedLeavingCircle.getCenterX();
		double slcCenterY = selectedLeavingCircle.getCenterY();
		Point2D slcCenterScene = selectedLeavingCircle.localToScene( new Point2D(slcCenterX, slcCenterY) );

		Optional<PickResult> pickResult = pickRectangle(
				slcCenterScene.getX(),
				slcCenterScene.getY()
		);

		Rectangle startingSquare = null;
		if( pickResult.isPresent() ) {
			startingSquare = pickResult.get().getSquare();
		}

		String startingSquareId = startingSquare.getId();
		String startingSquareCoords = StringUtils.substring(startingSquareId, 2, 5);
		String[] startingSquareToks = StringUtils.split(startingSquareCoords, "_");		
		Integer startingSqX = NumberUtils.createInteger( startingSquareToks[0] );
		Integer startingSqY = NumberUtils.createInteger( startingSquareToks[1] );
		
		String targetSquareId = targetSquare.getId();
		String targetSquareCoords = StringUtils.substring(targetSquareId, 2, 5);
		String[] targetSquareToks = StringUtils.split(targetSquareCoords, "_");		
		Integer targetSqX = NumberUtils.createInteger( targetSquareToks[0] );
		Integer targetSqY = NumberUtils.createInteger( targetSquareToks[1] );

		int distanceX = Math.abs(targetSqX - startingSqX);
		int distanceY = Math.abs(targetSqY - startingSqY);

		if( log.isLoggable( Level.FINE ) ) {
			log.fine("[IS DISTANCE] " + startingSquareId + " to " + targetSquareId );
			log.fine("[IS DISTANCE] distanceX=" + distanceX + ", distanceY=" + distanceY);
		}

		// 
		// Must be one and only one off in each axis
		//
		// This method does not consider direction (moving diagonally backwards
		// for non-kinged pieces)
		//
		if( distanceX != 1 || distanceY != 1 ) {
			return false;
		}
		
		return true;
	}
	
	private boolean isUnoccupied(Rectangle targetSquare) {			
		return !targetSquare.getProperties().containsKey(PROPERTY_GAMEPIECE);
	}

    private Rectangle findSquareById(String id) {
        for( Pane p : panes ) {
            for( Node cell : p.getChildrenUnmodifiable() ) {
                if( StringUtils.equalsIgnoreCase( cell.getId(), id ) ) {
                    return (Rectangle)cell;
                }
            }
        }
        return null;
    }

    private void moveToTray(Group gpToMove) {

		// TODO: put in move king to tray code

        SideType side = (SideType)gpToMove.getProperties().get(PROPERTY_SIDE);

        assert side != null;

        Circle trayCircle = (Circle)gpToMove.getChildrenUnmodifiable().get(INDEX_TRAY_CIRCLE);
        Circle circle = (Circle)gpToMove.getChildrenUnmodifiable().get(INDEX_CIRCLE);
        Circle leavingCircle = (Circle)gpToMove.getChildrenUnmodifiable().get(INDEX_LEAVING_CIRCLE);

		Point2D CP = new Point2D( circle.getCenterX(), circle.getCenterY() );
		Point2D CPS = circle.localToScene(CP);

		if( log.isLoggable(Level.FINE) ) {
			log.fine("[MOVE 2 TRAY] 1 moving id=" + circle.getId() + ", x=" + CPS.getX() + ", y=" + CPS.getY());
		}

        circle.setOpacity( 1.0d );
        leavingCircle.setOpacity( 1.0d );
        trayCircle.setOpacity( 0.0d );

        circle.setVisible(false);
        leavingCircle.setVisible(false);
        trayCircle.setVisible(true);

		Point2D circlePt = new Point2D( circle.getCenterX(), circle.getCenterY() );
		Point2D circlePtScene = circle.localToScene(circlePt);

		Optional<PickResult> pickResult = pickRectangle(circlePtScene.getX(), circlePtScene.getY());
		Rectangle onSquare = null;
		if( pickResult.isPresent() ) {
			onSquare = pickResult.get().getSquare();
		}

		if( log.isLoggable(Level.FINE) ) {
			log.fine("[MOVE 2 TRAY] removing id=" + circle.getId() + " from onSquare=" + onSquare.getId());
		}

		onSquare.getProperties().remove( PROPERTY_GAMEPIECE );

		Integer row = NumberUtils.createInteger( StringUtils.substring(onSquare.getId(), 2, 3) );
		panes.get(row).getChildren().remove( gpToMove );

		if( side.equals(SideType.RED) ) {

			if( boardOrientation.equals(OrientationType.RED_ON_TOP ) ) {
				topJumpTray.getChildren().add( gpToMove );
			} else {
				bottomJumpTray.getChildren().add( gpToMove );
			}

		} else {  // SideType.BLACK

			if( boardOrientation.equals(OrientationType.BLACK_ON_TOP ) ) {
				topJumpTray.getChildren().add( gpToMove );
			} else {
				bottomJumpTray.getChildren().add( gpToMove );
			}
		}


		final Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        timeline.setAutoReverse(false);
        timeline.getKeyFrames()
            .add(new KeyFrame(Duration.millis(1000),
                            new KeyValue(circle.opacityProperty(), 0.0d),
                            new KeyValue(leavingCircle.opacityProperty(), 0.0d),
                            new KeyValue(trayCircle.opacityProperty(), 1.0d)));
        timeline.play();

    }

	public void handleKeyCommand(KeyEvent evt) {

		switch( evt.getCode() ) {
			case E:

				if( aMoveWasMade ) {

					if (log.isLoggable(Level.FINE)) {
						log.fine("[END TURN]");
					}

					if (turn.get().equals(SideType.RED)) {
						turn.set(SideType.BLACK);
					} else {
						turn.set(SideType.RED);
					}

					aMoveWasMade = false;

					if( lastJumperGamePiece.isPresent() ) {
						lastJumperGamePiece = Optional.empty();
					}
				}
				break;
		}
	}
}
