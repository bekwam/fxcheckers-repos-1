/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.client;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main entry point for FXCheckers client
 *
 * @author carl
 *
 */
public class FXCheckersApp extends Application {

	private final Logger log = Logger.getLogger("FXCheckersApp");

	@Override
	public void start(Stage primaryStage) throws Exception {

		FXMLLoader fxmlLoader = new FXMLLoader(FXCheckersApp.class.getResource("/fxc-fxml/FXCheckers.fxml"));
		
		Parent p = fxmlLoader.load();
		
		FXCheckersController c = fxmlLoader.getController();
		
		Scene scene = new Scene(p);
		scene.getStylesheets().add("/fxc-css/fxc.css");
		scene.addEventFilter(KeyEvent.KEY_PRESSED, (evt) -> c.handleKeyCommand(evt));

		primaryStage.setTitle("FX Checkers");
		primaryStage.setScene( scene );
		primaryStage.setOnHidden( (evt) -> {
			try {
				c.disconnect();
			} catch(Exception exc) {
				log.log(Level.SEVERE, "error disconnecting", exc);
			}
		});
		
		primaryStage.show();
	}

	public static void main(String[] args) { launch(args); }
}
