package com.bekwam.fxc.client;

import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

/**
 * Transport object for a pick selection
 *
 * Includes board square (Rectangle) and row (Pane)
 *
 * @author carl
 */
public class PickResult {

    private final Rectangle square;
    private final Pane row;

    public PickResult(Rectangle square, Pane row) {
        this.square = square;
        this.row = row;
    }

    public Rectangle getSquare() {
        return square;
    }

    public Pane getRow() {
        return row;
    }
}
