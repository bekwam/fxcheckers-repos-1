/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.server;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.commons.lang3.StringUtils;

/**
 * @author carl
 *
 */
@ServerEndpoint("/echo")
public class ReverseEchoWebSocketServerEndpoint {

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	 
	private static final Set<Session> sessions = Collections.synchronizedSet(new HashSet<Session>());
	
    @OnOpen
    public void onConnectionOpen(Session session) {
        logger.info("Connection opened ... " + session.getId());
        sessions.add(session); 
    }
 
    @OnMessage
    public void onMessage(Session session, String message) throws IOException, EncodeException {
/*        if (StringUtils.isBlank(message)) {
            return "Please send message";
        }
        return StringUtils.reverse(message);
*/        
        for (Session openSession : sessions) {
            try {
                logger.info("received message=" + message + " sending to " + openSession.getId());
               openSession.getBasicRemote().sendObject(StringUtils.reverse(message));
            } catch (IOException | EncodeException ex) {
               sessions.remove(openSession);
            }
         }
        
        //
    }
 
    @OnClose
    public void onConnectionClose(Session session) {
        logger.info("Connection close .... " + session.getId());
        sessions.remove(session); 
    }
}
