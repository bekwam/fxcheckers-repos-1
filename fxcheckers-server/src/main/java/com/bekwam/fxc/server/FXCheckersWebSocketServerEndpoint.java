/*
 * Copyright 2016 Bekwam, Inc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.bekwam.fxc.server;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Logger;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.bekwam.fxc.domain.Message;
import com.bekwam.fxc.domain.NotifyFoundHostMessage;
import com.bekwam.fxc.domain.NotifyFoundOtherPlayerMessage;
import com.bekwam.fxc.domain.NotifyGameEndMessage;
import com.bekwam.fxc.domain.NotifyHostingMessage;
import com.bekwam.fxc.domain.RequestGameMessage;
import com.google.gson.Gson;

/**
 * Company-wide game server for connecting clients
 * 
 * @author carl
 *
 */
@ServerEndpoint("/fxc")
public class FXCheckersWebSocketServerEndpoint {

	private final Logger logger = Logger.getLogger(this.getClass().getName());
	 
	private static final List<Session> activeSessions = Collections.synchronizedList(new ArrayList<>());
	
	private static final Queue<Session> unmatchedSessions = new ConcurrentLinkedQueue<>();
	
	@OnOpen
    public void onConnectionOpen(Session session) {
        logger.info("Connection opened ... " + session.getId());
        activeSessions.add( session );
    }
 
    @OnMessage
    public void onMessage(String message, Session session) {
    	
   		logger.info( "[ON MESSAGE] unmatchedSessions hash=" + unmatchedSessions.hashCode());
    	
    	if( !session.getUserProperties().containsKey("screenName") ) {
    		
       		Message m = new Gson().fromJson( message, Message.class );
       		
       		if( m.getType().equals( Message.TYPE_REQUEST_GAME ) ) {
       			RequestGameMessage rgm = new Gson().fromJson( message, RequestGameMessage.class );
				logger.info( "[ON MESSAGE] assigning screenName to session; " + rgm.getScreenName() );			
       			session.getUserProperties().put( "screenName", rgm.getScreenName() );
       		}
    	}
    	
    	if( unmatchedSessions.isEmpty() ) {
    		
       		logger.info( "[ON MESSAGE] no one available for a game; adding a waiter");

        	boolean changed = unmatchedSessions.add( session );
    	
       		logger.info( "[ON MESSAGE] collection changed=" + changed);

       		NotifyHostingMessage msg = new NotifyHostingMessage();
       		String json = new Gson().toJson( msg );
			try {
				session.getBasicRemote().sendObject( json );
			} catch (Exception e) {
				e.printStackTrace();
			}

    	} else {
    		
       		logger.info( "[ON MESSAGE] there is someone in the queue");

       		//
    		// There is at least one session waiting for a game
    		//
    		// Dequeue
    		//
    		Session hostSession = unmatchedSessions.remove();
    		
    		//
    		// Form message for host
    		//
    		NotifyFoundOtherPlayerMessage forHost = 
    				new NotifyFoundOtherPlayerMessage( 
    						(String)session.getUserProperties().get("screenName") 
    						);
    		
    		String forHostJson = new Gson().toJson( forHost );
    		
    		NotifyFoundHostMessage toOtherPlayer = 
    				new NotifyFoundHostMessage( 
    						(String)hostSession.getUserProperties().get("screenName"),
    						"localhost",
    						8080
    						);
    		
    		String toOtherJson = new Gson().toJson( toOtherPlayer );

    		try {
    			
				hostSession.getBasicRemote().sendObject( forHostJson );			
				hostSession.getUserProperties().put("otherSession", session);
				
				session.getBasicRemote().sendObject( toOtherJson );
				session.getUserProperties().put("otherSession", hostSession);
				
    		} catch (Exception exc) {
				
    			exc.printStackTrace();
			}
    		
    	}    	
    }
 
    @OnClose
    public void onConnectionClose(Session session) {
    	
    	String screenName = "";
    	
    	if( session.getUserProperties().get("screenName") != null ) {
    		screenName = (String)session.getUserProperties().get("screenName");
    	}
        
   		logger.info( "[CLOSE] " + session.getId() + " from " + screenName);
        
   		Session otherSession = (Session)session.getUserProperties().get("otherSession");
   		if( otherSession != null ) {  // leaving in a game; clean up other session
   			
   			synchronized(otherSession) {
   				NotifyGameEndMessage gameEnd = new NotifyGameEndMessage();
   				String toGameEndJson = new Gson().toJson( gameEnd );
   				try {
   					otherSession.getBasicRemote().sendObject( toGameEndJson );
   					otherSession.getUserProperties().remove("otherSession");
   				} catch(Exception exc) {
   					exc.printStackTrace();
   				}
   			}
   			
   			activeSessions.remove(otherSession);
   		}
   		
    	if( unmatchedSessions.contains(session) ) {
        	unmatchedSessions.remove(session);
        }
        
    	activeSessions.remove( session );
    }
}
