<!DOCTYPE html>
<html>
<head>
<title>Echo Test</title>
<script>
var ws;

function doLoad() {
var wsUrl;
if (window.location.protocol == 'http:') {
    wsUrl = 'ws://' + window.location.host + '/fxcheckers-server/fxc';
} else {
    wsUrl = 'wss://' + window.location.host + ':8443/fxcheckers-server/fxc';
}
console.log('WebSockets Url : ' + wsUrl);
ws = new WebSocket(wsUrl);
 
ws.onopen = function(event){
    console.log('WebSocket connection started');
};
 
ws.onclose = function(event){
     console.log("Remote host closed or refused WebSocket connection");
     console.log(event);
};
 
ws.onmessage = function(event){
    console.log(event.data);
    document.getElementsByName('outputMessage')[0].value = event.data;
};
}

function send() {
  var message=document.getElementsByName('inputMessage')[0].value;
  console.log('Input message .. '+message);
  ws.send(message);
}
</script>

</head>

<body onload="doLoad()">
<h1>Echo Test</h1>

<form action="dummy">
	<input type="text" name="inputMessage">
	<input type="button" name="messageSubmit" value="Send" onclick="send()">
	<input type="text" name="outputMessage">
</form>

</body>

</html>